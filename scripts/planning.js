var http;

http = require("http");
_ = require("lodash");

var config = require('../config');

var spreadsheets = config.spreadsheets;
var spreadsheetId = config.spreadsheetId;
var username = config.username;
var password = config.password;

module.exports = function(robot) {
  return robot.respond(/(planning) (.*)/i, function(msg) {
    var query;
    query = msg.match[2];
    var names = query.split(" ");
    //console.log(query);return;
    var today = new Date();
    var date = today.getDate() + '/' + (today.getMonth() + 1);

    var Spreadsheet = require('edit-google-spreadsheet');
    for (var sheet in spreadsheets) {
      Spreadsheet.load({
        debug: true,
        spreadsheetId: spreadsheetId,
        worksheetId: spreadsheets[sheet],
        // Choose from 1 of the 3 authentication methods:
        //    1. Username and Password
        username: username,
        password: password,
      }, function sheetReady(err, spreadsheet) {
        if(err) throw err;
        spreadsheet.receive(function(err, rows, info) {
          if(err) throw err;
          // Find the date column
          //console.log(rows["2"]);
          var dateColumn = '';
          _.forEach(rows["2"], function(dateValue, dateIndex){
            if (dateValue == date) {
              dateColumn = dateIndex;
            }
          });
          for (var nameIndex in names) {
            name = names[nameIndex];
            var planning = '';
            _.forEach(rows, function(row, rowIndex) {
              var nameToCheck = row["1"];
              try {
                if (nameToCheck !== undefined && nameToCheck.toUpperCase() == name.toUpperCase()) {
                  msg.reply(name + ' is planned on ' + row[dateColumn]);
                }
                return;
              }
              catch (Exception){};
            });
          }
        });
      });
    }
  });
};
