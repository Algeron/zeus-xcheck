# Description:
#   catches the post hook from bitbucket
#
# Dependencies:
#   bitbucket
#
# Configuration:
#   configurate on bitbucket under hooks => POST hook then url = domain/hubot/bitbucket/roomid@
#   http://confluence.atlassian.com/display/BITBUCKET/Setting+Up+the+bitbucket+POST+Service
_ = require "underscore"

module.exports = (robot) ->
  robot.router.post '/hubot/bitbucket/:room', (req, res) ->
    room = req.params.room

    data = JSON.parse req.body.payload

    commits = data.commits

    s = if commits.length > 1 then "s" else ""
    msg = "#{commits.length} commit#{s} pushed to #{data.repository.name}:\n"

    for commit in commits
      msg += "  |#{commit.node}| : \"#{commit.message.replace(/(\r\n|\n|\r)/gm, '')}\" ON #{_.last(commits).branch} AT #{commit.timestamp} BY #{commit.author}\n"

    robot.messageRoom room, msg

    res.writeHead 204, { 'Content-Length': 0 }
    res.end()
